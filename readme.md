## Futility Forever

A silly port of a silly game. 

Originally written in Sol-20 BASIC by Robert Berry. Creatively reinterpreted in 2008-2010 by Ben Berry using Python.

## Running
Futility Forever uses the Pygame framework. Install it by running 

```bash
pip install pygame
```

Then, run the game with

```bash
python main.python
```

Enjoy! Or don't. It's really up to you. Be the change.

## Credits
Bomb sounds credit to Mike Koenig, http://soundbible.com
Alien sounds credit to WIM, http://www.freesound.org